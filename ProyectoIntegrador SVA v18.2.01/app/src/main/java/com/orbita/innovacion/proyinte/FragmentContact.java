package com.orbita.innovacion.proyinte;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class FragmentContact extends Fragment {

    String correo = "yagari2017@gmail.com", contrasena = "YAGARIINC2017";
    String a, b;

    EditText name, address, mensaje;
    Button envar;
    Session session;

    Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment_contact, container, false);

        a = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Nombre","");
        b = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Email","");

        name = (EditText) v.findViewById(R.id.edtname);
        address = (EditText) v.findViewById(R.id.edtcorreo);
        mensaje = (EditText) v.findViewById(R.id.edtmensaje);
        envar = (Button) v.findViewById(R.id.btmenviar);

        name.setText(a);
        address.setText(b);

        envar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                envar.setEnabled(false);

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                Properties properties = new Properties();
                properties.put("mail.smtp.host", "smtp.googlemail.com");
                properties.put("mail.smtp.socketFactory.port", "465");
                properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                properties.put("mail.smtp.auth", "true");
                properties.put("mail.smtp.port", "465");

                try {
                    session=Session.getDefaultInstance(properties, new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(correo,contrasena);
                        }
                    });

                    if(session != null){
                        javax.mail.Message message = new MimeMessage(session);
                        message.setFrom(new InternetAddress(correo));
                        message.setSubject(a + " " + b);
                        message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse("rodriguez231402@hotmail.com"));
                        message.setContent(mensaje.getText().toString(), "text/html; charset=utf-8");
                        Transport.send(message);
                    }

                    Toast.makeText(context, "Comentario enviado", Toast.LENGTH_SHORT).show();

                    name.setText("");
                    address.setText("");
                    mensaje.setText("");

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        return v;
    }

}
